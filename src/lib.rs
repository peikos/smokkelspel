#[macro_use]
extern crate seed;

extern crate rand;
extern crate web_sys;

use crate::rand::prelude::IteratorRandom;
use crate::rand::{SeedableRng, Rng};
use seed::prelude::*;
use std::collections::HashSet;
use rand::rngs::StdRng;

use wasm_timer::SystemTime;

#[derive(Clone, Debug)]
pub struct Player {
    name: String,
    seed: usize,
    post_below: f64,
    douane_above: f64,
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub enum Role {
    Douane,
    Post,
    Player,
}

impl Player {
    fn post(&self) -> String {
        format!("{} handelt in {}.", self.name, self.element())
    }

    fn douane(&self) -> String {
        format!("{} is douane!", self.name)
    }

    fn player(&self) -> String {
        format!("{} is vrij om te handelen.", self.name)
    }

    pub fn role_sentence(&self, t: u64) -> String {
        match self.role(t) {
            Role::Post => self.post(),
            Role::Douane => self.douane(),
            Role::Player => self.player(),
        }
    }

    pub fn new_role_sentence(&self, t: u64) -> Option<String> {
        let role = self.role(t);
        if (t == 0 && role == self.role(119)) || role == self.role(t - 1) {
            Some(match self.role(t) {
                Role::Post => self.post(),
                Role::Douane => self.douane(),
                Role::Player => self.player(),
            })
        } else {
            None
        }
    }

    //let mut players = ["Abel", "Margje", "Joshua", "Terje", "Tom", "Emily", "Paul", "Jesper", "Stijn", "Mart", "Marius", "Kees", "Lasse"]
    pub fn element(&self) -> String {
        String::from(match self.name.as_ref() {
            "Kees" => "Keeson (geel)",
            "Abel" => "Abelium",
            "Margje" => "Margnesium (rood)",
            "Joshua" => "Joshuine (bruin)",
            "Terje" => "Terjestof (blauw)",
            "Tom" => "Tomidomite",
            "Emily" => "Emelide (paars)",
            "Paul" => "Paulladium (groen)",
            "Jesper" => "Jesperium (wit)",
            "Stijn" => "Stijnstof (grijs)",
            "Mart" => "Martanium (goud)",
            "Marius" => "Marium",
            "Lasse" => "Lassaat (oranje)",
            _ => "Oopsium",
        })
    }

    pub fn role(&self, t: u64) -> Role {
        let y = func(self.seed, t);
        if y < self.post_below {
            Role::Post
        } else if y > self.douane_above {
            Role::Douane
        } else {
            Role::Player
        }
    }
}

#[derive(Clone, Debug)]
pub struct Model {
    players: Vec<Player>,
}

pub struct Recept {
    ingredients: Vec<usize>,
    price: u64,
}

impl Recept {
    pub fn new(s: usize) -> Recept {
        let mut rng = StdRng::seed_from_u64(s as u64);
        let n = rng.gen_range(2,4);
        let ingredients = (0..n).map(|_| (0..NUM)
                               .choose(&mut rng)
                               .unwrap())
            .collect::<Vec<usize>>();
        let mut r = Recept { ingredients, price: 0 };
        r.price = r.price(s as u64);
        r

    }

    pub fn show(&self, players: &Vec<Player>) -> String {
        format!("Een mix van {} is {} waard.",
            self.ingredients.iter().map(|n| players[*n].element()).collect::<Vec<_>>().join(", "),
            self.price)
    }

    pub fn price(&self, time: u64) -> u64 {
        return (self.ingredients.len() as u64
            * self
                .ingredients
                .iter()
                .collect::<HashSet<_>>()
                .iter()
                .count() as u64
            * 3)
            * (10 + time);
    }
}

pub struct Msg;

fn update(_msg: Msg, _model: &mut Model, _orders: &mut impl Orders<Msg>) {}

fn view(model: &Model) -> Node<Msg> {
    let t = SystemTime::now()
        .duration_since(SystemTime::UNIX_EPOCH)
        .unwrap()
        .as_secs()
        % 7200      // Reset every 2 hours
        / 120; // Units of 2 minutes

    let recepten: Vec<Recept> = (0..6).map(|n| Recept::new(n + t as usize)).collect();

    div![
        p![tt![format!("t = {:?}", t)]],
        div![recepten.iter().map(|recept| p![tt![recept.show(&model.players)]])],
        div![
            model.players.iter().map(|p| p![tt![p.new_role_sentence(t)]])
        ],
        h1![tt!["Alle rollen"]],
        div![
            model.players.iter().map(|p| p![tt![p.role_sentence(t)]])
        ],
        /*div![
            style! {
                St::Display => "flex",
                St::MaxWidth => "100%"
            },
            model.players.iter().map(|player| {
                div![
                    style! {
                        St::Border => "1px solid black",
                        St::Padding => px(10),
                        St::Margin => px(10),
                    },
                    p![tt![format!("{:?}", player)]],
                    p![tt![format!("{:?}", func(player.seed, t))]],
                    p![tt![format!("{}: {:?}", player.name, player.role(t))]]
                ]
            })
        ]*/
    ]
}

fn func(seed: usize, t: u64) -> f64 {
    0.3 * (-3.2 * f64::sin(-0.13 * t as f64 + seed as f64)
        - 1.2 * f64::sin(-0.17 * std::f64::consts::E * t as f64 + seed as f64)
        + 1.9 * f64::sin(0.0035 * seed as f64 * std::f64::consts::PI * t as f64 + seed as f64))
}

const NUM: usize = 8;
const PLAYERS: [&str; NUM] = [ "Margje", "Joshua", "Terje", "Paul", "Jesper", "Stijn", "Mart", "Kees" ];

fn init(_url: Url, _orders: &mut impl Orders<Msg>) -> Model {
    let players = PLAYERS
    .iter()
    .enumerate()
    .map(|(seed, player)| {
        let mut vals: Vec<f64> = (0..120).map(|x| func(seed, x)).collect();
        vals.sort_by(|a, b| a.partial_cmp(b).unwrap());
        let post_below = vals[30];
        let douane_above = vals[105];
        Player {
            name: player.to_string(),
            seed,
            post_below,
            douane_above,
        }
    })
    .collect();

    Model { players }
}

#[wasm_bindgen(start)]
pub fn render() {
    seed::App::start("app", init, update, view);
}
